const expect = require('chai').expect
const Grape = require('grenache-grape').Grape
const { Wind } = require ('../src/wind')
const sinon = require('sinon')

const grapeOne = new Grape({
  // host: '127.0.0.1', // if undefined the Grape binds all interfaces
  dht_port: 20001,
  dht_bootstrap: [
    '127.0.0.1:20002'
  ],
  api_port: 30001
})

const grapeTwo= new Grape({
  // host: '127.0.0.1', // if undefined the Grape binds all interfaces
  dht_port: 20002,
  dht_bootstrap: [
    '127.0.0.1:20001'
  ],
  api_port: 40001
})

const wait = async (ms) => {
  return new Promise((resolve) => {
    setTimeout(resolve, ms)
  })
}

describe('Wind', () => {
  before((done) => {
    grapeOne.start(() => {
      grapeTwo.start(done)
    })
  })

  after((done) => {
    grapeOne.stop(() => {
      grapeTwo.stop(done)
    })
  })

  describe('incomingOrderExists', () => {
    it('returns true if passed order/message already exists locally', () => {
      const wind = new Wind()
      const stub = sinon.stub(wind, 'externalOrders')
      stub.returns([ { id: '12345' } ])
      expect(wind.incomingOrderExists({ payload: { id: '12345'} })).to.equal(true)
    })

    it('returns false if passed oder/message does not exist', () => {
      const wind = new Wind()
      const stub = sinon.stub(wind, 'externalOrders')
      stub.returns([])
      expect(wind.incomingOrderExists({ payload: { id: '12345'} })).to.equal(false)
    })
  })

  describe('createdFromThisNode', () => {
    it('returns true if nodeID from message matches generated nodeID', () => {
      const wind = new Wind()
      wind.nodeID = 'reddit'
      expect(wind.createdFromThisNode({ payload: { nodeID: 'reddit' } })).to.equal(true)
    })

    it('returns false if nodeID from message does not match generated nodeID', () => {
      const wind = new Wind()
      wind.nodeID = 'voat'
      expect(wind.createdFromThisNode({ payload: { nodeID: 'reddit' } })).to.equal(false)
    })
  })

  describe('limitOrder()', () => {
    it('should set an offerID / type / nodeID for the order', () => {
      const wind = new Wind()
      const fakeOrder = {test: 'hello world'}
      expect(wind.orders).to.deep.equal([])
      wind.limitOrder(fakeOrder)
      expect(wind.orders.length).to.equal(1)
      const order = wind.orders[0]
      expect(order.nodeID).to.equal(wind.nodeID)
      expect(order.id).to.exist
    })

  })

  it('should broadcast orders to other users', async () => {
    const windOne = new Wind()
    const windTwo = new Wind()
    const message =  { symbol: 'reddit' }
    expect(windTwo.externalOrders().length).to.equal(0)
    expect(windTwo.externalOrders()).to.deep.equal([])
    windOne.limitOrder(message)
    await wait(15000)
    console.log(windTwo.externalOrders())
    expect(windTwo.externalOrders().length).to.equal(1)
    // expect(windTwo.offers[0]).to.deep.equal(message)

  });

});
