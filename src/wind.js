
// const express = require('express')
// const session = require('express-session')
// const flash = require('flash')
const Crypto = require('crypto')
const Link = require('grenache-nodejs-link')
const { PeerPub, PeerSub } = require('grenache-nodejs-ws')

class Wind {
  constructor() {
    this.tickRate = 500
    this.nodeID = Crypto.randomBytes(16).toString('hex')
    this.orders = [] // all known orders
    this.setupGrapeLink()
    this.setupPublisher()
    this.setupSubscriber()
    setInterval(() => {
      this.internalOrders().forEach(order => {
        this.service.pub(JSON.stringify({ payload: order, type: 'order' }))
      })
    }, this.tickRate)
  }



  internalOrders() {
    return this.orders.filter(order => order.nodeID === this.nodeID)
  }

  externalOrders() {
    return this.orders.filter(order => order.nodeID !== this.nodeID)
  }


  initializeLink() {
    const link = new Link({
      grape: 'http://127.0.0.1:30001'
    })
    link.start()
  }

  setupSubscriber() {
    this.subscriber = new PeerSub(this.link, {})
    this.subscriber.init()
    setInterval(() => {
      this.subscriber.sub('pub_test', { timeout: 5000 })
    }, this.tickRate)
    this.registerHandlers()
  }

  setupPublisher() {
    this.publisher = new PeerPub(this.link, {})
    this.publisher.init()
    this.service = this.publisher.transport('server')
    this.service.listen(1024 + Math.floor(Math.random() * 1000))
    setInterval(() => {
      this.link.announce('pub_test', this.service.port, {})
    }, this.tickRate)
  }

  setupGrapeLink() {
    this.link = new Link({
      grape: 'http://127.0.0.1:30001'
    })
    this.link.start()
  }

  registerHandlers() {
    this.subscriber.on('error', (err) => {
      if (err.message === 'ERR_GRAPE_LOOKUP_EMPTY') {
        console.error('Error connecting to other users. Retrying shortly.')
      }
    })
    this.subscriber.on('message', (message) => {
      const msg = JSON.parse(message)
      switch (msg.type) {
        case 'order':
          this.processOrder(msg)
          break;
      }
    })
  }

  // handleMessage(message) {
  //   const decodedMsg = JSON.parse(message)
  //   console.log('rofl')
  //     if (decodedMsg.nodeID === this.nodeID) {
  //       // drop messages from self
  //       return
  //     }
  //     console.log('msg received!', decodedMsg)
  //     switch (decodedMsg.type) {
  //       case 'offer':
  //         this.processOffer(decodedMsg)
  //         break;
  //     }
  // }

  limitOrder(order) {
    this.orders.push({ ...order, id: Crypto.randomBytes(16).toString('hex'), nodeID: this.nodeID })
  }

  incomingOrderExists(message) {
    return !!this.externalOrders().find(order => order.id === message.payload.id)
  }

  createdFromThisNode(message) {
    return message.payload.nodeID === this.nodeID
  }

  validOrder(message) {
    // Valid offer if we don't already have it
    return !this.incomingOrderExists(message) && !this.createdFromThisNode(message)
  }

  processOrder(message) {
    if (this.validOrder(message)) {
      this.orders.push(message.payload)
    }
  }
}

// const wind = new Wind()

// const app = express()
// app.set('view engine', 'ejs')
// app.set('views', 'src/views')
// app.use(express.urlencoded({ extended: true }));
// app.use(session({
//   secret: 'ethfinex-test',
// }));
// app.use(flash());
// app.get('/', (req, res) => {
//   const orders = wind.offers
//   res.render('home', { title: 'Hey', message: 'Hello there!' })
// })

// app.post('/submit-offer', (req, res) => {
//   wind.limitOrder({ type: 'offer', offer: { ...req.body } })
//   req.flash('info', 'Your order has been submitted successfully');
//   res.redirect('/')
// })

// app.get('/offers', (req, res) => {
//   debugger
//   res.json(wind.offers)
// })


// app.listen(process.env.PORT || 3000, () => console.log('Wind started!'))
// wind.limitOrder({ nodeID: wind.nodeID, type: process.env.MESSAGE })

exports.Wind = Wind
